prime_numbers = [2, ]
prime_factors = []

n = 3


def get_prime_factors(number):
    global n

    for i in prime_numbers:
        if n > number:
            return
        # n is not a prime number
        if (n / i).is_integer():
            continue

        if n != prime_numbers[-1]:
            prime_numbers.append(n)

        if (number / n).is_integer():
            if not number / n == n:
                prime_factors.append(n)
                return get_prime_factors(number / n)

        n += 1

# get_prime_factors(13195)
get_prime_factors(600851475143)  # 0.209 seconds to run
print(prime_factors)
