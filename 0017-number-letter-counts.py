NUMBER_WORD_MAPPING = {
    '1': 'one',
    '2': 'two',
    '3': 'three',
    '4': 'four',
    '5': 'five',
    '6': 'six',
    '7': 'seven',
    '8': 'eight',
    '9': 'nine',
    '10': 'ten',
    '11': 'eleven',
    '12': 'twelve',
    '13': 'thirteen',
    '14': 'fourteen',
    '15': 'fifteen',
    '16': 'sixteen',
    '17': 'seventeen',
    '18': 'eighteen',
    '19': 'nineteen',
    '20': 'twenty',
    '30': 'thirty',
    '40': 'forty',
    '50': 'fifty',
    '60': 'sixty',
    '70': 'seventy',
    '80': 'eighty',
    '90': 'ninety'
}


def number_length(num_string):
    return len(NUMBER_WORD_MAPPING[num_string])


def count_letter(final_number):
    letters_count = 0

    for i in range(1, final_number + 1):
        num_string = str(i)
        num_length = len(num_string)

        if num_string in NUMBER_WORD_MAPPING:
            letters_count += number_length(num_string)

        elif num_length == 2:
            if num_string in NUMBER_WORD_MAPPING:
                letters_count += number_length(num_string)
            else:
                letters_count += number_length(num_string[0] + '0') + \
                    number_length(num_string[1])

        elif num_length == 3:
            letters_count += number_length(num_string[0]) + len('hundred')
            if num_string[1:3] in NUMBER_WORD_MAPPING:
                letters_count += len('and')
                letters_count += number_length(num_string[1:3])
            elif num_string[1] + '0' in NUMBER_WORD_MAPPING:
                letters_count += len('and')
                letters_count += number_length(num_string[1] + '0') + \
                    number_length(num_string[2])
            elif num_string[2] in NUMBER_WORD_MAPPING:
                letters_count += len('and')
                letters_count += number_length(num_string[2])

        else:
            letters_count += number_length('1') + len('thousand')

    return letters_count


count = count_letter(5)
print(count)
assert count == 19

print(count_letter(1000))
