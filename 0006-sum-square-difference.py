def sum_squares(range):
    sum = 0

    for i in range:
        sum += i * i

    return sum


def square_sum(range):
    square = 0
    sum = 0

    for i in range:
        sum += i

    square = sum * sum

    return square

# sum = sum_squares(range(10, 0, -1))
# square = square_sum(range(10, 0, -1))
#
# diff = square - sum
# print(diff)  # 0.000 seconds to run

sum = sum_squares(range(100, 0, -1))
square = square_sum(range(100, 0, -1))

diff = square - sum
print(diff)  # 0.000 seconds to run
