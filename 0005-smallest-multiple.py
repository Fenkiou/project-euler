def smallest_multiple(range):
    multiple = 0
    is_integer = False

    while not is_integer:
        multiple += 1

        for i in range:
            if not (multiple / i).is_integer():
                is_integer = False
                break

            is_integer = True

    print(multiple)

# smallest_multiple(range(10, 1, -1))  # 0.001 seconds to run
smallest_multiple(range(20, 1, -1))  # 84.683 seconds to run
