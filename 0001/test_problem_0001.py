from problem_0001 import multiple_3_and_5, sum_divisible_by


def test_result_for_10():
    max_number = 10
    result = 23

    assert multiple_3_and_5(max_number) == result
    assert sum_divisible_by(3, max_number) + sum_divisible_by(5, max_number) == 23


def test_result_for_1000():
    max_number = 1000
    result = 233168

    assert multiple_3_and_5(max_number) == result
    assert (
        sum_divisible_by(3, max_number)
        + sum_divisible_by(5, max_number)
        - sum_divisible_by(15, max_number)
        == result
    )


def test_result_for_1000000():
    max_number = 1000000
    result = 233333166668

    # Very slow...
    # assert multiple_3_and_5(max_number) == result
    assert (
        sum_divisible_by(3, max_number)
        + sum_divisible_by(5, max_number)
        - sum_divisible_by(15, max_number)
        == result
    )


def test_result_for_1000000000():
    max_number = 1000000000
    result = 233333333166666680

    # Very very slow...
    # assert multiple_3_and_5(max_number) == result
    assert (
        sum_divisible_by(3, max_number)
        + sum_divisible_by(5, max_number)
        - sum_divisible_by(15, max_number)
        == result
    )
