import math


def multiple_3_and_5(max_number):
    sum_ = 0

    for i in range(0, max_number):
        if i % 3 == 0 or i % 5 == 0:
            sum_ += i

    return sum_


def sum_divisible_by(number, target):
    p = math.floor((target - 1) / number)

    return math.floor(number * (p * (p + 1)) / 2)
