def largest_product(number):
    greatest_product = 0

    for i in range(0, len(number)):
        adjacents = number[i:i + 13]

        if len(adjacents) < 13:
            break

        if "0" in adjacents:
            continue

        product = 1

        for j in adjacents:
            product *= int(j)

        if product > greatest_product:
            greatest_product = product

    return greatest_product

f = open('data/8.txt', 'r')
data = ''.join(f.read().splitlines())
f.close()

print(largest_product(data))  # 0.002 seconds to run
