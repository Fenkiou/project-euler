f = open('data/13.txt', 'r')
data = f.readlines()
f.close()

sum = 0
for digit in data:
    sum += int(digit.rstrip())

print(str(sum)[0:10])
