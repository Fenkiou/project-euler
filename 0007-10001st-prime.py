def get_prime_numbers(limit):
    prime_numbers = [2, ]
    current = 3

    while len(prime_numbers) != limit:
        for i in prime_numbers:
            if (current / i).is_integer():
                break

            if i == prime_numbers[-1]:
                prime_numbers.append(current)
                break

        current += 1

    return prime_numbers

prime_numbers = get_prime_numbers(10001)

print(prime_numbers[-1])  # 16.544 seconds to run
