def is_palindrome(n):
    return str(n) == str(n)[::-1]


def largest_palindrome_product():
    largest_product = 0

    for i in range(900, 999):
        for j in range(900, 999):
            value = i * j
            if value > largest_product:
                if is_palindrome(value):
                    largest_product = value

    print(largest_product)

largest_palindrome_product()  # 0.009 seconds to run
