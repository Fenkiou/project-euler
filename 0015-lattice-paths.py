def factorial(n):
    result = n
    for i in range(n - 1, 1, -1):
        result *= i

    return result


def get_nb_paths(cells, moves):
    cells_fact = factorial(cells)
    moves_fact = factorial(moves)
    cells_less_moves_fact = factorial(cells - moves)

    nb_paths = cells_fact / (moves_fact * cells_less_moves_fact)

    print(nb_paths)

rows = 20
cols = 20
moves = int(round((rows + cols) / 2))

get_nb_paths(rows + cols, moves)
