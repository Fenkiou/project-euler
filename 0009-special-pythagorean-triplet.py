def find_pythagorean_triplet(number):
    for a in range(3, number + 1):
        for b in range(4, number - a + 1):
            for c in range(5, number - b + 1):
                if a + b + c > number:
                    break

                if a * a + b * b == c * c:
                    print(a, b, c)

                    if a + b + c == number:
                        return a, b, c

a, b, c = find_pythagorean_triplet(1000)
print(a, b, c)
print(a * b * c)
