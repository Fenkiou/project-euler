import math

number_found = False
n = 1
triangle_number = None

while not number_found:
    triangle_number = (n * (n + 1)) / 2

    nb_factors = 0
    square_root = int(math.sqrt(triangle_number)) + 1

    for i in range(1, square_root):
        if triangle_number % i == 0:
            nb_factors += 2

    n += 1

    if nb_factors >= 500:
        number_found = True

print(triangle_number)
