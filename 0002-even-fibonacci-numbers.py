def even_fibonacci_numbers(max_value):
    a = 1
    b = 1
    sum = 0

    while b < max_value:
        if b % 2 == 0:
            sum += b

        tmp = a + b
        a = b
        b = tmp

    print(sum)

even_fibonacci_numbers(4000000)  # 0.000 seconds to run
