import math


def sieve_of_eratosthenes(n):
    sieve = {key: True for key in range(2, n + 1)}

    for i in range(2, int(math.sqrt(n)) + 1):
        if sieve[i]:
            for j in range(i * i, n + 1, i):
                sieve[j] = False

    prime_numbers = [k for k, v in sieve.items() if v is True]

    return prime_numbers


def prime_sum(number):
    prime_numbers = sieve_of_eratosthenes(number)
    sum = 0

    for p in prime_numbers:
        sum += p

    return sum

print("sum", prime_sum(2000000))
