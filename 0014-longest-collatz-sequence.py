def get_sequence(sequence, n):
    sequence.append(n)
    if n == 1:
        return sequence
    if n % 2 == 0:
        return get_sequence(sequence, n / 2)
    else:
        return get_sequence(sequence, (3 * n) + 1)

highest_seq = []
n = 0
for i in range(13, 1000001):
    seq = []
    current_seq = get_sequence(seq, i)

    if len(current_seq) > (len(highest_seq)):
        highest_seq = list(current_seq)
        n = i

print(highest_seq)
print(n)
