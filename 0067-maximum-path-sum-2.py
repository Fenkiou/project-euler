def parse_file(file_path):
    with open(file_path, 'r') as f:
        raw_data = f.read().splitlines()

    rows = []
    for raw_row in raw_data:
        numbers = raw_row.split(' ')
        rows.append([int(number) for number in numbers])

    return rows


def maximum_total_top_to_bottom(rows):
    rows.reverse()
    sum_ = 0

    for i, row in enumerate(rows):
        for j in range(len(row)):
            try:
                rows[i + 1][j] = max(
                    [rows[i + 1][j] + row[j], rows[i + 1][j] + row[j + 1]]
                )
            except IndexError:
                sum_ = row[0]

    return sum_


rows = parse_file('data/67-real.txt')
sum_ = maximum_total_top_to_bottom(rows)
print(sum_)
assert sum_ == 7273
