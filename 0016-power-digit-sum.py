def sum_digit(number):
    sum_value = 0

    for digit in str(number):
        sum_value += int(digit)

    return sum_value


print(sum_digit(2**15))
print(sum_digit(2**1000))
