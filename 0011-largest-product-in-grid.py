f = open('data/11.txt', 'r')
data = f.read().replace(' ', '\n').splitlines()
f.close()

grid, tmp = [], []
col = 0

for d in data:
    tmp.append(d)
    col += 1

    if col == 20:
        grid.append(tmp.copy())
        tmp.clear()
        col = 0


largest_product = 0


def compute_product(a, b, c, d):
    result = int(a) * int(b) * int(c) * int(d)

    if result == 1788696:
        print(a, b, c, d)

    global largest_product
    if result > largest_product:
        largest_product = result


def right(grid, r, c):
    w = grid[r][c]
    x = grid[r][c + 1]
    y = grid[r][c + 2]
    z = grid[r][c + 3]

    compute_product(w, x, y, z)


def left(grid, r, c):
    w = grid[r][c]
    x = grid[r][c - 1]
    y = grid[r][c - 2]
    z = grid[r][c - 3]

    compute_product(w, x, y, z)


def down(grid, r, c):
    w = grid[r][c]
    x = grid[r + 1][c]
    y = grid[r + 2][c]
    z = grid[r + 3][c]

    compute_product(w, x, y, z)


def up(grid, r, c):
    w = grid[r][c]
    x = grid[r - 1][c]
    y = grid[r - 2][c]
    z = grid[r - 3][c]

    compute_product(w, x, y, z)


def diag_down_right(grid, r, c):
    w = grid[r][c]
    x = grid[r + 1][c + 1]
    y = grid[r + 2][c + 2]
    z = grid[r + 3][c + 3]

    compute_product(w, x, y, z)


def diag_down_left(grid, r, c):
    w = grid[r][c]
    x = grid[r + 1][c - 1]
    y = grid[r + 2][c - 2]
    z = grid[r + 3][c - 3]

    compute_product(w, x, y, z)


def diag_up_right(grid, r, c):
    w = grid[r][c]
    x = grid[r - 1][c + 1]
    y = grid[r - 2][c + 2]
    z = grid[r - 3][c + 3]

    compute_product(w, x, y, z)


def diag_up_left(grid, r, c):
    w = grid[r][c]
    x = grid[r - 1][c - 1]
    y = grid[r - 2][c - 2]
    z = grid[r - 3][c - 3]

    compute_product(w, x, y, z)

for r in range(len(grid)):
    for c in range(len(grid[r])):
        if r <= 2 and c <= 2:
            right(grid, r, c)
            down(grid, r, c)
            diag_down_right(grid, r, c)

        elif r <= 2 and (c >= 3 and c <= 16):
            right(grid, r, c)
            left(grid, r, c)
            down(grid, r, c)
            diag_down_right(grid, r, c)
            diag_down_left(grid, r, c)

        elif r <= 2 and c >= 17:
            left(grid, r, c)
            down(grid, r, c)
            diag_down_left(grid, r, c)

        elif (r >= 3 and r <= 16) and c <= 2:
            right(grid, r, c)
            up(grid, r, c)
            down(grid, r, c)
            diag_down_right(grid, r, c)
            diag_up_right(grid, r, c)

        elif (r >= 3 and r <= 16) and (c >= 3 and c <= 16):
            left(grid, r, c)
            right(grid, r, c)
            up(grid, r, c)
            down(grid, r, c)
            diag_down_right(grid, r, c)
            diag_down_left(grid, r, c)
            diag_up_right(grid, r, c)
            diag_up_left(grid, r, c)

        elif (r >= 3 and r <= 16) and c >= 17:
            left(grid, r, c)
            up(grid, r, c)
            down(grid, r, c)
            diag_down_left(grid, r, c)
            diag_up_left(grid, r, c)

        elif r >= 17 and c <= 2:
            right(grid, r, c)
            up(grid, r, c)
            diag_up_right(grid, r, c)

        elif r >= 17 and (c >= 3 and c <= 16):
            left(grid, r, c)
            up(grid, r, c)
            right(grid, r, c)
            diag_up_left(grid, r, c)
            diag_up_right(grid, r, c)

        else:
            left(grid, r, c)
            up(grid, r, c)
            diag_up_left(grid, r, c)

print(largest_product)
